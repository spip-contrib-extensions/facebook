<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'facebook_titre' => 'Facebook',
	'titre_page_facebook_poster' => 'Poster sur Facebook',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'compte_post' => 'Compte utilisé pour afficher les posts ?',
	'confirmer_enregistrer_compte_post' => 'Enregistrement réussi',

	// E
	'erreur_enregistrer_compte_post' => 'Echec d\'enregistrement',

	// T
	'titre_page_configurer_facebook' => 'Configuration de Facebook',
	'compte_connecte' => 'Compte actuellement connecté: @compte@',
	'cle' => "Identifiant de l'app",
	'secret' => 'Clé secrète',
	'message' => 'Message',
	'lien' => 'Lien',
	'page' => 'Poster sur un page ?',
	'confirmer_poster' => 'Message correctement posté sur Facebook',
	'publication' => 'Publication sur Facebook',
	'titre_compte_boite' => 'Compte Facebook',
	'titre_connecter_facebook' => 'Connecter un compte facebook au site'
);
