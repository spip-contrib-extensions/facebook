<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'facebook_description' => 'Poster sur Facebook depuis SPIP',
	'facebook_nom' => 'Facebook',
	'facebook_slogan' => 'Intégration de Facebook à SPIP',
);